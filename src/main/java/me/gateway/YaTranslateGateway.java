package me.gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import me.entities.Translate;





public class YaTranslateGateway {
    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String TOKEN = "";

    public Translate getTranslate(String texts, String sourceLanguageCode, String targetLanguageCode) throws UnirestException {
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.post(URL)
                .header("Accept", "*/*")
                .header("key", TOKEN)
                .field("texts", texts)
                .field("sourceLanguageCode", sourceLanguageCode)
                .field("targetLanguageCode", targetLanguageCode)
                .asString();
        String strResponse = response.getBody();
        return gson.fromJson(strResponse, Translate.class);
    }

}
