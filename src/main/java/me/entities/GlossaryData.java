package me.entities;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GlossaryData {

    @SerializedName("glossaryPairs")
    private List<GlossaryPair> glossaryPairs = null;

//    public List<GlossaryPair> getGlossaryPairs() {
//        return glossaryPairs;
//    }
//
//    public void setGlossaryPairs(List<GlossaryPair> glossaryPairs) {
//        this.glossaryPairs = glossaryPairs;
//    }

}
