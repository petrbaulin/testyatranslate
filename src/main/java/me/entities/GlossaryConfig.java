
package me.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GlossaryConfig {

    @SerializedName("glossaryData")
    private GlossaryData glossaryData;

//    public GlossaryData getGlossaryData() {
//        return glossaryData;
//    }
//
//    public void setGlossaryData(GlossaryData glossaryData) {
//        this.glossaryData = glossaryData;
//    }

}
