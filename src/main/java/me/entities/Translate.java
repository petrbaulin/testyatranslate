
package me.entities;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Translate {

    @SerializedName("sourceLanguageCode")
    private String sourceLanguageCode;
    @SerializedName("targetLanguageCode")
    private String targetLanguageCode;
    @SerializedName("format")
    private String format;
    @SerializedName("texts")
    private List<String> texts = null;
    @SerializedName("folderId")
    private String folderId;
    @SerializedName("model")
    private String model;
    @SerializedName("glossaryConfig")
    private GlossaryConfig glossaryConfig;

//    public String getSourceLanguageCode() {
//        return sourceLanguageCode;
//    }
//
//    public void setSourceLanguageCode(String sourceLanguageCode) {
//        this.sourceLanguageCode = sourceLanguageCode;
//    }
//
//    public String getTargetLanguageCode() {
//        return targetLanguageCode;
//    }
//
//    public void setTargetLanguageCode(String targetLanguageCode) {
//        this.targetLanguageCode = targetLanguageCode;
//    }
//
//    public String getFormat() {
//        return format;
//    }
//
//    public void setFormat(String format) {
//        this.format = format;
//    }
//
//    public List<String> getTexts() {
//        return texts;
//    }
//
//    public void setTexts(List<String> texts) {
//        this.texts = texts;
//    }
//
//    public String getFolderId() {
//        return folderId;
//    }
//
//    public void setFolderId(String folderId) {
//        this.folderId = folderId;
//    }
//
//    public String getModel() {
//        return model;
//    }
//
//    public void setModel(String model) {
//        this.model = model;
//    }
//
//    public GlossaryConfig getGlossaryConfig() {
//        return glossaryConfig;
//    }
//
//    public void setGlossaryConfig(GlossaryConfig glossaryConfig) {
//        this.glossaryConfig = glossaryConfig;
//    }

}
