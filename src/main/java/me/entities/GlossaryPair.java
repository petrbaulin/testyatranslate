
package me.entities;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class GlossaryPair {

    @SerializedName("sourceText")
    private String sourceText;
    @SerializedName("translatedText")
    private String translatedText;

//    public String getSourceText() {
//        return sourceText;
//    }
//
//    public void setSourceText(String sourceText) {
//        this.sourceText = sourceText;
//    }
//
//    public String getTranslatedText() {
//        return translatedText;
//    }
//
//    public void setTranslatedText(String translatedText) {
//        this.translatedText = translatedText;
//    }

}
